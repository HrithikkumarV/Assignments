//
//  AppDelegate.swift
//  Assignments
//
//  Created by Hrithik Kumar V on 30/05/22.
//

import Cocoa

@main
class AppDelegate: NSObject, NSApplicationDelegate {

    

    private var window : NSWindow!
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        
       
        window = NSWindow(contentRect: .zero, styleMask: [.resizable,.closable,.miniaturizable,.titled], backing: .buffered, defer: false)
        let vc = SplitViewAssignmentViewController()
        vc.splitView = AssignmentSplitView()
        vc.splitView.isVertical  = true
        window.contentViewController = vc
        window.minSize = NSSize(width: 900, height: 600)
        window.title = "Split View"
        window.center()
        window.makeKeyAndOrderFront(nil)
        
       
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    func applicationSupportsSecureRestorableState(_ app: NSApplication) -> Bool {
        return true
    }
    
   



}

