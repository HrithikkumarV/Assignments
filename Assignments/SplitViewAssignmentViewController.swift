//
//  ViewController.swift
//  Assignments
//
//  Created by Hrithik Kumar V on 30/05/22.
//

import Cocoa

class SplitViewAssignmentViewController :  NSSplitViewController, NSWindowDelegate, customSplitItemDelegate{
    
    
    
    lazy var sv1 : customSplitItem = {
        let vc = DummyVCForSplitViewController()
        let sv1  = customSplitItem(sidebarWithViewController: vc)
        sv1.delegate = self
        sv1.minimumThickness = 80
        sv1.maximumThickness = 80
        sv1.canCollapse = false
        sv1.preferredThicknessFraction = 0.3
        return sv1
    }()
    
    lazy var subSV1 : customSplitItem = {
        let vc = DummyVCForSplitViewController()
        
        vc.theme.backgroundColour = NSColor.systemPink.cgColor
        let sv1  = customSplitItem(viewController: vc)
        sv1.delegate = self
        sv1.minimumThickness = 300
        sv1.canCollapse = true
        return sv1
    }()
    
    lazy var subSV2 : customSplitItem = {
        let vc = DummyVCForSplitViewController()
        
        vc.theme.backgroundColour = NSColor.systemRed.cgColor
        let sv2  = customSplitItem(viewController: vc)
        sv2.delegate = self
        sv2.minimumThickness = 300
        sv2.canCollapse = true
        return sv2
    }()
    
    
    lazy var subSplitViewController : NSSplitViewController = {
        let vc = NSSplitViewController()
        vc.splitViewItems = [subSV1 , subSV2]
        vc.splitView.isVertical = false
        return vc
    }()
    
    lazy var sv2 : customSplitItem = {
        let sv2  = customSplitItem(viewController: subSplitViewController)
        sv2.delegate = self
        sv2.minimumThickness = 300
        sv2.canCollapse = true
        return sv2
    }()
    
    lazy var sv3 : customSplitItem = {
        
        let vc = DummyVCForSplitViewController()
        
        vc.theme.backgroundColour = NSColor.systemBlue.cgColor
        let sv3  = customSplitItem(viewController: vc)
        sv3.delegate = self
        sv3.minimumThickness = 300
        return sv3
    }()
    
    
    override func viewDidLoad() {
        addSplitViewItem(sv1)
        addSplitViewItem(sv2)
        addSplitViewItem(sv3)
        super.viewDidLoad()
    }
    
    
    
   
    
    override func splitView(_ splitView: NSSplitView, effectiveRect proposedEffectiveRect: NSRect, forDrawnRect drawnRect: NSRect, ofDividerAt dividerIndex: Int) -> NSRect {
        if(dividerIndex == 0){
            return .zero
        }
        return proposedEffectiveRect
    }
    

    
    
    func didSplitItemCollapse(_ splitItem: NSSplitViewItem) {
        if(subSV2 == splitItem){
            sv2.canCollapse = false
        }
    }
   
}





class DummyVCForSplitViewController : NSViewController{
    var theme : Theme = Theme()
    
   
    override func loadView() {
        view = NSView()
        view.wantsLayer = true
        view.layer?.backgroundColor = theme.backgroundColour
    }
    
    struct Theme{
        var backgroundColour : CGColor = .clear
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
    }
    
    
    
    
}




class AssignmentSplitView : NSSplitView{

    override var dividerThickness: CGFloat {
        get {
            return 0
        }
    }
    
    
    
    
    
    
    init(){
        super.init(frame: .zero)
        dividerStyle = .thin
        self.setValue(NSColor(calibratedRed: 0, green: 0, blue: 0, alpha: 0), forKey: "dividerColor")
        self.wantsLayer = true
        self.layer?.backgroundColor = .clear
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   

    
}

protocol customSplitItemDelegate : AnyObject{
    func didSplitItemCollapse(_ splitItem : NSSplitViewItem)
}


class customSplitItem : NSSplitViewItem{
    
    weak var delegate : customSplitItemDelegate?
    
    override var isCollapsed: Bool{
        didSet{
            delegate?.didSplitItemCollapse(self)
        }
    }
    
}
